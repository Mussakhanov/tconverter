//
//  ViewController.swift
//  TConverter
//
//  Created by Тимур on 25/06/2019.
//  Copyright © 2019 Timur Mussakhanov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var cellsiusLabel: UILabel!
    @IBOutlet weak var fahrenheitLabel: UILabel!
    @IBOutlet weak var slider: UISlider! {
        didSet {
            slider.maximumValue = 100
            slider.minimumValue = 0
            slider.value = 0
        }
    }
    
    
    @IBAction func sliderValueChenged(_ sender: UISlider){
        let temperatureCelsius = Int(round(sender.value))
        cellsiusLabel.text = "\(temperatureCelsius)ºC"
        
        let temperatureFahrenheit = Int(round((sender.value * 9 / 5) + 32))
        fahrenheitLabel.text = "\(temperatureFahrenheit)ºF"
    }

    
    @IBAction func reset(_ sender: UIButton) {
        slider.value = 0
        cellsiusLabel.text = "0ºC"
        fahrenheitLabel.text = "0ºF"
    }
    
}

